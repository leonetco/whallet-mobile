var Helpers = (function(lng, undefined){

    checkValidLocale = function(){
      var validLocales = ["es", "en", "zh", "ja", "ko", "ru"];                
      if(validLocales.indexOf(String.locale)==-1){
          String.locale = "en";
      }          
      return String.locale;
    };

    updateAllTranslateElement = function(){
      Lungo.dom('#aside_my_whallet')[0].innerHTML = l("%my_whallet");    
      Lungo.dom('#aside_movements')[0].innerHTML = l("%movements");
      Lungo.dom('#aside_settings')[0].innerHTML = l("%settings");
      Lungo.dom('#aside_share')[0].innerHTML = l("%share");
      Lungo.dom('#aside_distribution')[0].innerHTML = l("%distribution");
      Lungo.dom('#button_signout')[0].innerHTML = "<span class='icon linecons li_lock'></span>"+ l("%signout");
      

      Lungo.dom('#title_share')[0].innerHTML = l("%share");
      Lungo.dom('#connect_follow_us')[0].innerHTML = l("%follow_us");
      Lungo.dom('#connect_like_us')[0].innerHTML = l("%like_us");
      Lungo.dom('#connect_suggest_us')[0].innerHTML = l("%suggest_us");
      Lungo.dom('#connect_score_in_play')[0].innerHTML = l("%score_in_play");
      Lungo.dom('#connect_suggest_to_grow')[0].innerHTML = l("%suggest_to_grow");
      Lungo.dom('#title_select_category')[0].innerHTML = l("%select_category");
      Lungo.dom('#select_category_home')[0].innerHTML = l("%category_home");
      Lungo.dom('#select_category_bills')[0].innerHTML = l("%category_bills");
      Lungo.dom('#select_category_shop')[0].innerHTML = l("%category_shop");
      Lungo.dom('#select_category_food')[0].innerHTML = l("%category_food");
      Lungo.dom('#select_category_education')[0].innerHTML = l("%category_education");
      Lungo.dom('#select_category_transport')[0].innerHTML = l("%category_transport");
      Lungo.dom('#select_category_entertaiment')[0].innerHTML = l("%category_entertaiment");
      Lungo.dom('#select_category_personal')[0].innerHTML = l("%category_personal");
      Lungo.dom('#select_category_health')[0].innerHTML = l("%category_health");
      Lungo.dom('#select_category_other')[0].innerHTML = l("%category_other");

      Lungo.dom('#new_amount_cancel')[0].innerHTML = l("%cancel");
      Lungo.dom('#accept')[0].innerHTML = l("%accept");

      Lungo.dom('#connections_tweet')[0].innerHTML = l("%tweet");
      Lungo.dom('#connections_like_fb')[0].innerHTML = l("%like_fb");
      Lungo.dom('#connections_score')[0].innerHTML = l("%score");


      return Lungo.dom('#title_new_amount')[0].innerHTML = l("%new_amount");

    }

    getTranslate = function(key){
      return l("%"+key);
    }

    getMonthShortNames = function(){
        return new Array(
            getTranslate("abbr_month_names_1"),
            getTranslate("abbr_month_names_2"),
            getTranslate("abbr_month_names_3"),
            getTranslate("abbr_month_names_4"),
            getTranslate("abbr_month_names_5"),
            getTranslate("abbr_month_names_6"),
            getTranslate("abbr_month_names_7"),
            getTranslate("abbr_month_names_8"),
            getTranslate("abbr_month_names_9"),
            getTranslate("abbr_month_names_10"),
            getTranslate("abbr_month_names_11"),
            getTranslate("abbr_month_names_12")); 
    }

    getMonthNames = function(){
        return new Array(
            getTranslate("month_names_0"),
            getTranslate("month_names_1"),
            getTranslate("month_names_2"),
            getTranslate("month_names_3"),
            getTranslate("month_names_4"),
            getTranslate("month_names_5"),
            getTranslate("month_names_6"),
            getTranslate("month_names_7"),
            getTranslate("month_names_8"),
            getTranslate("month_names_9"),
            getTranslate("month_names_10"),
            getTranslate("month_names_11")); 
    }

    getDayNames = function(){
      return new Array(
        getTranslate("day_names_0"),
        getTranslate("day_names_1"),
        getTranslate("day_names_2"),
        getTranslate("day_names_3"),
        getTranslate("day_names_4"),
        getTranslate("day_names_5"),
        getTranslate("day_names_6"));
    }

    miclick = function(){
        console.log("click");
    };

    to_currency = function(amount, decPlaces, thouSeparator, decSeparator){
      var n = amount,
      decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
      decSeparator = decSeparator == undefined ? "." : decSeparator,
      thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
      sign = n < 0 ? "-" : "",
      i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
      j = (j = i.length) > 3 ? j % 3 : 0;
      return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");      
    }

    to_currency_user = function(amount){    
      symbol = "€";
      decimal_mark = ",";
      thousands_separator = ".";
      decPlaces = 2;
      symbol_first = false;

      user = Lungo.Data.Storage.persistent("userInfo");
      if( user != null){
        if( user.currency != null){
          symbol = user.currency.symbol == undefined ? user.currency.iso_code : user.currency.symbol;
          decimal_mark = user.currency.decimal_mark;
          thousands_separator = user.currency.thousands_separator;
          symbol_first = user.currency.symbol_first;
        }
      }

      format_amount = to_currency(amount, decPlaces, thousands_separator, decimal_mark);
      format_currency = symbol_first ? symbol + " " + format_amount : format_amount + " " + symbol;

      return format_currency

    }

    userCurrencySymbol = function(){      
      user = Lungo.Data.Storage.persistent("userInfo");            
      if (user !== null) {
        return user.currency == null ? "€" : user.currency.symbol;
      }
    }

    getCategoryName = function(category) {
        var category_name;
        if (category === "1" || category === 1) {
            category_name = "recibo";
        } else if (category === "2" || category === 2) {
            category_name = "compra";
        } else if (category === "3" || category === 3) {
            category_name = "food";
        } else if (category === "4" || category === 4) {
            category_name = "educacion";
        } else if (category === "5" || category === 5) {
            category_name = "transporte";
        } else if (category === "6" || category === 6) {
            category_name = "ocio";
        } else if (category === "7" || category === 7) {
            category_name = "personal";
        } else if (category === "8" || category === 8) {
            category_name = "salud";
        } else if (category === "9" || category === 9) {
            category_name = "casita";
        } else if (category === "10" || category === 10) {
            category_name = "add-money";
        } else {
            category_name = "wtag";
        }
        return category_name;
    };

    getCategoryId = function(category) {
      var category_id;
      if (category === "recibo") {
        category_id = 1;
      } else if (category === "compra") {
        category_id = 2;
      } else if (category === "food") {
        category_id = 3;
      } else if (category === "educacion") {
        category_id = 4;
      } else if (category === "transporte") {
        category_id = 5;
      } else if (category === "ocio") {
        category_id = 6;
      } else if (category === "personal") {
        category_id = 7;
      } else if (category === "salud") {
        category_id = 8;
      } else if (category === "casita") {
        category_id = 9;
      } else if (category === "add-money") {
        category_id = 10;
      } else {
        category_id = 0;
      }
      return category_id;
    };

    getFormatDate = function(date) {
      var m_names = getMonthNames();
      return m_names[date.getMonth()] + " " + date.getFullYear();
    };

    getLongFormatDate = function(date){
      dd = date.getDate();
      mm = date.getMonth() + 1;
      yyyy = date.getFullYear();

      dd = ("0" + dd).slice(-2);
      mm = ("0" + mm).slice(-2);


      date = dd + '/' + mm + '/' + yyyy;
      return date;
    };

    getElegantDate = function(date) {
        day = date.getDay();
        number_day = date.getDate();
        month = date.getMonth();
        year = date.getFullYear();
        var m_names = getMonthNames();
        var d_names = getDayNames();

        return d_names[day] + ", " + number_day + " " + m_names[month] + " " + year;
    };

    getFormatNumber = function(number, currency) {
        return to_currency_user(number);
        //return accounting.formatMoney(number, "", 2, ".", ",") + " " + currency;
    };

    to_date = function(valueDate){
        var separatorDate = "/";
        var fechaCalendario = new Date();
        if(valueDate === ""){ fechaCalendario = new Date(); }
        else{
            valueDateSplit = valueDate.split(separatorDate);
            fechaCalendario = new Date(valueDateSplit[2], valueDateSplit[1]-1, valueDateSplit[0]);
        }
        return fechaCalendario;
    }


    getTemplateSelectDayMonthYear = function(date){
        yearCalendarMobile  = date.getFullYear();
        monthCalendarMobile = date.getMonth();
        dayCalendarMobile   = date.getDate();

        //Variables String del calendario
        var m_names = getMonthNames();
        textMonthCalendarMobile = m_names[monthCalendarMobile].slice(0,3);
        textDayCalendarMobile   = ("0"+dayCalendarMobile).slice(-2);

        template  = new String('<div id="modal_calendario">');
        template += '     <form>';
        template += '     <fieldset style="border-bottom: 2px solid #0093D5">';
        template += '         <span id="screen-date" style="font-size: 1.4em; height:40px; margin-top:15px" class="text thin left"><abbr>'+ Helpers.getElegantDate(date) +'</abbr></span>';
        template += '     </fieldset>';
        template += '     <div>';
        template += '         <div id="contenedor_day_calendario" class="fecha_calendario fecha_calendario_left">';
        template += '             <div class="contenedor_signo_calendario">';
        template += '                 <div id="masDayCalendarMobile" class="signo_calendario" onclick=Calendar.setDateScreen(Calendar.addDay())><span class="icon android aup"></span></div>';
        template += '             </div>';
        template += '             <div class="contenedor_numero_calendario">';
        template += '                 <div id="dayCalendarMobile" class="numero_calendario" style="margin-top: 20px">'+ textDayCalendarMobile +'</div>';
        template += '             </div>';
        template += '             <div class="contenedor_signo_calendario">';
        template += '                 <div id="menosDayCalendarMobile" class="signo_calendario" onclick=Calendar.setDateScreen(Calendar.removeDay())><span class="icon android adown"></span></div>';
        template += '             </div>';
        template += '         </div>';
        template += '         <div id="contenedor_month_calendario" class="fecha_calendario">';
        template += '             <div class="contenedor_signo_calendario">';
        template += '                 <div id="masMonthCalendarMobile" class="signo_calendario" onclick=Calendar.setDateScreen(Calendar.addMonth())><span class="icon android aup"></span></div>';
        template += '             </div>';
        template += '             <div class="contenedor_numero_calendario">';
        template += '                 <div id="monthCalendarMobile" class="numero_calendario" style="margin-top: 20px">'+ textMonthCalendarMobile +'</div>';
        template += '             </div>';
        template += '             <div id="anterior_month" class="contenedor_signo_calendario">';
        template += '                 <div id="menosMonthCalendarMobile" class="signo_calendario" onclick=Calendar.setDateScreen(Calendar.removeMonth())><span class="icon android adown"></span></div>';
        template += '             </div>';
        template += '         </div>';
        template += '         <div id="contenedor_year_calendario" class="fecha_calendario fecha_calendario_right">';
        template += '             <div class="contenedor_signo_calendario">';
        template += '                 <div id="masYearCalendarMobile" class="signo_calendario" onclick=Calendar.setDateScreen(Calendar.addYear())><span class="icon android aup"></span></div>';
        template += '             </div>';
        template += '             <div class="contenedor_numero_calendario">';
        template += '                 <div id="yearCalendarMobile" class="numero_calendario" style="margin-top: 20px">'+ yearCalendarMobile +'</div>';
        template += '             </div>';
        template += '             <div class="contenedor_signo_calendario">';
        template += '                 <div id="menosYearCalendarMobile" class="signo_calendario" onclick=Calendar.setDateScreen(Calendar.removeYear())><span class="icon android adown"></span></div>';
        template += '             </div>';
        template += '         <div style="clear:both;"></div>';
        template += '         </div>';
        template += '     </div>';
        template += ' </div></form>';

        return template;
    }

    getTemplateSelectMonthYear = function(date) {
      yearCalendarMobile = date.getFullYear();
      monthCalendarMobile = date.getMonth();
      var m_names = getMonthNames();
      textMonthCalendarMobile = m_names[monthCalendarMobile].slice(0, 3);
      
      template = new String('<div id="modal_calendario">');
      template += '     <form style="width: 100%">';
      template += '     <fieldset style="border-bottom: 2px solid #0093D5">';
      template += '                 <span id="screen-date" style="font-size: 1.4em; height:40px; margin-top:15px" class="text thin left"><abbr>' + Helpers.getFormatDate(date) + '</abbr></span>';
      template += '     </fieldset>';
      template += '     <div>';
      template += '     <div style="margin: 0px auto; width: 145px;">';
      template += '         <div id="contenedor_day_calendario" class="hidden fecha_calendario fecha_calendario_left">';
      template += '             <div class="contenedor_signo_calendario">';
      template += '                 <div id="masDayCalendarMobile" class="signo_calendario" onclick=Calendar.addDay()><span class="icon android aup"></span></div>';
      template += '             </div>';
      template += '             <div class="contenedor_numero_calendario">';
      template += '                 <div id="dayCalendarMobile" class="numero_calendario">1</div>';
      template += '             </div>';
      template += '             <div class="contenedor_signo_calendario">';
      template += '                 <div id="menosDayCalendarMobile" class="signo_calendario" onclick=Calendar.removeDay()><span class="icon android adown"></span></div>';
      template += '             </div>';
      template += '         </div>';
      template += '         <div id="contenedor_month_calendario" class="fecha_calendario">';
      template += '             <div class="contenedor_signo_calendario">';
      template += '                 <div id="masMonthCalendarMobile" class="signo_calendario" onclick=Calendar.setShortDateScreen(Calendar.addMonth())><span class="icon android aup"></span></div>';
      template += '             </div>';
      template += '             <div class="contenedor_numero_calendario">';
      template += '                 <div id="monthCalendarMobile" class="numero_calendario" style="margin-top: 20px">' + textMonthCalendarMobile + '</div>';
      template += '             </div>';
      template += '             <div id="anterior_month" class="contenedor_signo_calendario">';
      template += '                 <div id="menosMonthCalendarMobile" class="signo_calendario" onclick=Calendar.setShortDateScreen(Calendar.removeMonth())><span class="icon android adown"></span></div>';
      template += '             </div>';
      template += '         </div>';
      template += '         <div id="contenedor_year_calendario" class="fecha_calendario fecha_calendario_right">';
      template += '             <div class="contenedor_signo_calendario">';
      template += '                 <div id="masYearCalendarMobile" class="signo_calendario" onclick=Calendar.setShortDateScreen(Calendar.addYear())><span class="icon android aup"></span></div>';
      template += '             </div>';
      template += '             <div class="contenedor_numero_calendario">';
      template += '                 <div id="yearCalendarMobile" class="numero_calendario" style="margin-top: 20px">' + yearCalendarMobile + '</div>';
      template += '             </div>';
      template += '             <div class="contenedor_signo_calendario">';
      template += '                 <div id="menosYearCalendarMobile" class="signo_calendario" onclick=Calendar.setShortDateScreen(Calendar.removeYear())><span class="icon android adown"></span></div>';
      template += '             </div>';
      template += '         </div>';
      template += '         <div style="clear:both;"></div>';
      template += '     </div>';
      template += '     </div>';
      template += ' </div></form>';

      return template;
    }

    getTemplateSelectNumber = function(number) {
      
      template = new String('<div id="modal_calendario">');
      template += '     <form style="width: 100%">';
      template += '     <div>';
      template += '     <div style="margin: 0px 80px; width: 145px;">';
      template += '         <div id="contenedor_selector_number" class="fecha_calendario fecha_calendario_right">';
      template += '             <div class="contenedor_signo_calendario">';
      template += '                 <div id="masNumber" class="signo_calendario" onclick="Lungo.dom(\'div#yearCalendarMobile\')[0].innerText = parseInt(Lungo.dom(\'div#yearCalendarMobile\')[0].innerText)+1"><span class="icon android aup"></span></div>';
      template += '             </div>';
      template += '             <div class="contenedor_numero_calendario">';
      template += '                 <div id="yearCalendarMobile" class="numero_calendario" style="margin-top: 20px">' + number + '</div>';
      template += '             </div>';
      template += '             <div class="contenedor_signo_calendario">';
      template += '                 <div id="menosNumber" class="signo_calendario" onclick="if(parseInt(Lungo.dom(\'div#yearCalendarMobile\')[0].innerText) > 0){ Lungo.dom(\'div#yearCalendarMobile\')[0].innerText = parseInt(Lungo.dom(\'div#yearCalendarMobile\')[0].innerText)-1}"><span class="icon android adown"></span></div>';
      template += '             </div>';
      template += '         </div>';
      template += '         <div style="clear:both;"></div>';
      template += '     </div>';
      template += '     </div>';
      template += ' </div></form>';

      return template;
    }    

    return {
        getTranslate: getTranslate,
        getMonthShortNames  : getMonthShortNames,
        getMonthNames    : getMonthNames,
        getCategoryName  : getCategoryName,
        getCategoryId    : getCategoryId,
        getFormatDate    : getFormatDate,
        getElegantDate   : getElegantDate,
        getLongFormatDate : getLongFormatDate,
        getFormatNumber  : getFormatNumber,
        getTemplateSelectNumber: getTemplateSelectNumber,
        getTemplateSelectDayMonthYear : getTemplateSelectDayMonthYear, 
        getTemplateSelectMonthYear  : getTemplateSelectMonthYear,
        userCurrencySymbol : userCurrencySymbol,
        to_currency : to_currency,
        to_currency_user : to_currency_user,
        miclick          : miclick,
        updateAllTranslateElement : updateAllTranslateElement,
        checkValidLocale:  checkValidLocale,
        to_date : to_date
    };

})(Lungo);