var WhalletBalances = (function(lng, undefined){
    loadFromStorage = function(){
    	balances = Lungo.Data.Storage.persistent("balances");   
    	parseBalance = JSON.parse(balances);

      __Model.Balance.destroyAll();
      for (_i = 0, _len = parseBalance.balances.length; _i < _len; _i++) {
        balance = parseBalance.balances[_i];
        balanceModel = new __Model.Balance;
        balanceModel.date = balance.date;
        balanceModel.expense = balance.expense;
        balanceModel.income = balance.income;
        balanceModel.balance = balance.balance;
        balanceModel.save();
      }

      return parseBalance.balances;
    };

    saveToStorage = function(){
      var balance, balances, _len4, _m, _ref4, balanceModel, json3;
      balances = [];
      _ref4 = __Model.Balance.all();
      for (_m = 0, _len4 = _ref4.length; _m < _len4; _m++) {
        balance = _ref4[_m]

        balanceModel = new __Model.Balance;
        balanceModel.date = balance.date;
        balanceModel.expense = balance.expense;
        balanceModel.income = balance.income;
        balanceModel.balance = balance.balance;
        balances.push(balanceModel);
      }      
      Lungo.Data.Storage.persistent("balances",  JSON.stringify({ balances: balances }));
    }


    return {
        loadFromStorage: 	loadFromStorage,
        saveToStorage: 		saveToStorage
    };

})(Lungo);

var WhalletTags = (function(lng, undefined){
    loadFromStorage = function(){
      tags = Lungo.Data.Storage.persistent("tags");      
      parseTags = JSON.parse(tags);

      __Model.Tag.destroyAll();
      for (_i = 0, _len = parseTags.tags.length; _i < _len; _i++) {
        tag = parseTags.tags[_i];
        tagModel = __Model.Tag.create({
                    description: tag.description,
                    category: tag.category
                  });
      }

      return parseTags.tags;
    };

    saveToStorage = function(){
      var tag, tags, _len4, _m, _ref4, tagModel;
      tags = [];
      _ref4 = __Model.Tag.all();
      for (_m = 0, _len4 = _ref4.length; _m < _len4; _m++) {
        tag = _ref4[_m]

        tagModel = new __Model.Tag;
        tagModel.description = tag.description;
        tagModel.category = tag.category;

        tags.push(tagModel);
      }      
      Lungo.Data.Storage.persistent("tags",  JSON.stringify({ tags: tags }));
    }


    return {
        loadFromStorage:  loadFromStorage,
        saveToStorage:    saveToStorage
    };

})(Lungo);


var WhalletMovements = (function(lng, undefined){
    loadFromResponse = function(response){
      __Model.Movement.destroyAll();
      _results = [];
      currencySymbol = Helpers.userCurrencySymbol();
      for (_i = 0, _len = response.length; _i < _len; _i++) {
        movement = response[_i];
        description = movement['description'] ? movement['description'] : l('%no_description');
        completeDateSplit = movement['date'].split('T');
        dateSplit = completeDateSplit[0].split('-');
        date = new Date(dateSplit[0], dateSplit[1] - 1, dateSplit[2]);
        actualFormatDate = Helpers.getElegantDate(date);
        type = "expense";
        if (movement.category === 10 || movement.category === "10") {
          type = "income";
        }

        var has_recurrence = false;
        rule = __Model.MovementRecurrence.RULES.NEVER;
        endson = __Model.MovementRecurrence.ENDSON.FOREVER;
        endson_input = "";

        if(movement["movementRecurrence_id"] != null){
          has_recurrence = true;
          var movementRecurrence = movement['movementRecurrence'];
          rule = movementRecurrence['rule'];

          endson =  movementRecurrence['endson'];
          if(endson === __Model.MovementRecurrence.ENDSON.COUNT){
            endson = __Model.MovementRecurrence.ENDSON.COUNT;
            endson_input = movementRecurrence['number_repeat'];
          }
          if(endson === __Model.MovementRecurrence.ENDSON.UNTIL){
            endson = __Model.MovementRecurrence.ENDSON.UNTIL;
            endTimeSplit = movementRecurrence['end_time'].split('T');
            endTimeDateSplit = endTimeSplit[0].split('-');
            endson_input = Helpers.getLongFormatDate(new Date(endTimeDateSplit[0], endTimeDateSplit[1] - 1, endTimeDateSplit[2]));
          }

        }



        _results.push(movementModel = __Model.Movement.create({
          description: description,
          category: Helpers.getCategoryName(movement['category']),
          categoryID: movement['category'],
          date: date,
          formatDate: actualFormatDate,
          tags: movement['tags'],
          amount: Helpers.getFormatNumber(movement['amount'], currencySymbol),
          amountDouble: movement['amount'],
          type: type,
          done: false,
          has_recurrence: has_recurrence,
          rule: rule,
          rule_locale: Helpers.getTranslate("recurrence_"+rule),
          endson: endson,
          endson_input: endson_input,
          id: movement['_id']
        }));
      }
      console.log(_results);
      return _results;
    };

    return {
        loadFromResponse:  loadFromResponse
    };

})(Lungo);