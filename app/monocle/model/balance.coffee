class __Model.Balance extends Monocle.Model

    @fields "expense", "income", "balance", "date"