class __Model.MovementRecurrence extends Monocle.Model

    @fields "rule", "rule_locale", "endson", "endson_input"

    @RULES =
        NEVER    : "nunca"
        DAILY    : "daily"
        MONTHLY  : "monthly"
        YEARLY   : "yearly"
        
    @ENDSON =
        FOREVER    : 0
        COUNT    : 1
        UNTIL    : 2