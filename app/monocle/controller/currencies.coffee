class CurrenciesCtrl extends Monocle.Controller

    events:
        "load section#currencies"    : "onLoadCurrencies"


    constructor: ->
        super        

    onLoadCurrencies: (event) ->
        console.log "onLoadCurrencies"

        currencies = Lungo.Data.Storage.persistent("currencies")

        if currencies isnt null
            currenciesResponse currencies
        else
            url = "http://www.whallet.com/api/v1/helper/currencies.json"
            post_data = 
                    locale: User.getUserInfo().locale
            result = Lungo.Service.get(url, post_data, currenciesResponse)

    currenciesResponse= (response) ->
        console.log "currenciesResponse"

        currencyActive = User.getCurrencyActive()

        # clear list
        $$("ul#currencies").html(' ')

        for currency in response

            example = Helpers.to_currency(2164.49, 2, currency['thousands_separator'], currency['decimal_mark'])

            if currency['symbol_first']
                example = currency['symbol'] + " " + example
            else
                example = example + " " + currency['symbol']            
        
            currencyModel = __Model.Currency.create                
                name: currency['name'] + " - " + currency['iso_code'],
                symbol: currency['symbol'],
                example: example,
                id: currency['_id']
                active: false

            if currencyModel.id == currencyActive._id
                currencyModel.active = true

            view = new __View.CurrencyItemList model: currencyModel
            view.append currencyModel


controller_movements = new CurrenciesCtrl "section#currencies"