class NewMovementCtrl extends Monocle.Controller

    events:
        "load section#NewMovement"  : "onLoadNewMovement"
        "tap nav.groupbar a"        : "onChangeKindOf"
        "tap a[data-action=save]"   : "onSave"
        "tap a[data-action=cancel]" : "onCancel"
        "tap a[data-action=delete]" : "onDelete"
        "tap div#recurrency"        : "onRecurrence"
        "tap span.reload"           : "onRecurrence"

    elements:
        "#amount"               : "amount"
        "#category"             : "category"
        "#txt-tag"              : "tags"
        "#txt-date"             : "date"
        "#txt-description"      : "description"
        "#title_movement"       : "title_movement"
        "#income"               : "movement_income"
        "#expense"              : "movement_expense"
        "#movement_when"        : "movement_when"
        "#movement_more_info"   : "movement_more_info"
        "#movement_cancel"      : "movement_cancel"
        "#movement_accept"      : "movement_accept"
        "#location_icon"        : "location_icon"
        "#movement_latitude"    : "movement_latitude"
        "#movement_longitude"   : "movement_longitude"

    constructor: ->
        super
        console.log "Constructor"

    onRecurrence: (event) ->
        console.log "onRecurrence"
        Lungo.Router.section("newRecurrency")

    onDelete: (event) ->
        console.log "onDelete"
        url = "http://www.whallet.com/api/v1/movement/delete.json"
        movementID = Lungo.Data.Storage.persistent("movementID")

        # TODO que pasa si no tenemos movementID porque se haya dado de alta en memoria pero todavia no tenemos el ID
        post_data =
            token: Lungo.Data.Storage.persistent("tokenAuth"),
            id: movementID

        Lungo.Service.Settings.error = errorResponse
        result = Lungo.Service.post(url, post_data, deleteResponse)


        for movement in __Model.Movement.all()
            if movement.id == movementID
                movement.destroy()

        # actualizamos el balance del mes con los movimientos
        
        dateSelectedFormat = Helpers.getFormatDate(new Date(Lungo.Data.Storage.persistent("dateSelected")))
        for dataBalance in __Model.Balance.all()
            dateSplit = dataBalance.date.split('-')
            dateBalance = new Date(dateSplit[0], dateSplit[1] - 1, dateSplit[2])
            dataBalanceFormat = Helpers.getFormatDate(dateBalance)

            if(dataBalanceFormat == dateSelectedFormat)
                console.log "balance encontrado"
               
                dataBalance.income = 0
                dataBalance.expense = 0
                for movement in __Model.Movement.all()
                    if movement.categoryID == 10
                        dataBalance.income += movement.amountDouble
                    else
                        dataBalance.expense += movement.amountDouble

                dataBalance.balance = dataBalance.income - dataBalance.expense
                dataBalance.save()

        WhalletBalances.saveToStorage()

        Lungo.Notification.success(
            Helpers.getTranslate('success'),
            Helpers.getTranslate("movement_deleted"),
            "check",
            1,
            afterNotification 
        )

    onLoadNewMovement: (event) ->
        console.log "onLoadNewMovement"
        
        @tags[0].placeholder = l("%txttag.placeholder")
        @title_movement[0].innerHTML = l("%movement")
        @movement_income[0].innerHTML = l("%income")
        @movement_expense[0].innerHTML = l("%expense")
        @movement_when[0].innerHTML = l("%when")
        @movement_more_info[0].innerHTML = l("%more_info")
        @movement_cancel[0].innerHTML = l("%cancel")
        @movement_accept[0].innerHTML = l("%accept")

        categoryClass = Lungo.dom("img#category")[0].className.split(" ")
        categoryId = Helpers.getCategoryId(categoryClass[2])

        Lungo.dom("div#tags")[0].innerHTML = ""
        Lungo.dom("ul#bullets")[0].innerHTML = ""
        bulletsList = ""
        bulletsList = "<li class='prev'><a href='#' data-action='carousel' data-direction='left' data-icon='left' onclick= carousel.prev()><span class='icon left'></span></a></li>"

        tags = __Model.Tag.all()
        tagsList = ""
        first = true
        tagsByCategory = []
        for tag in tags
            if tag.category == categoryId
                tagsByCategory.push tag.description
        tagsByCategory = tagsByCategory.sort()
        numberTagsByCategory = tagsByCategory.length

        while tagsByCategory.length
            smallTagsList = tagsByCategory.splice(0,6)
            if first
                bulletsList += "<li class='selected'><span class='bullet'>o</span></li>"
            else
                bulletsList += "<li class=''><span class='bullet'>o</span></li>"
            first = false
            tagsList += "<div align='center'>"            
            for tag in smallTagsList
                tagsList += "<a href='#' class='button-tag' onclick=\"document.getElementById('txt-tag').value='"+tag+"'\">"+ tag + "</a>"                
            tagsList += '</div>'

        Lungo.dom("ul#bullets")[0].style.cssText = ""
        if numberTagsByCategory < 6        
            Lungo.dom("ul#bullets")[0].style.cssText = "display:none"
        else
            Lungo.dom("ul#bullets")[0].style.cssText = "display: block;margin:10px"

        bulletsList += "<li class='next'><a href='#' data-action='carousel' data-direction='right' data-icon='right' onclick=carousel.next()><span class='icon right'></span></a></li>"
        Lungo.dom("ul#bullets")[0].innerHTML = bulletsList
        Lungo.dom("div#tags")[0].innerHTML = tagsList
        


    onCancel: (event) ->
        console.log "onCancel"
        Lungo.Data.Storage.persistent("movementID", null)
        Lungo.Router.back()

    onSave: (event) ->
        console.log "onSave"

        currencySymbol = Helpers.userCurrencySymbol()
        user = Lungo.Data.Storage.persistent("userInfo")
        
        date = @date[0].value
        if date == ""
            today = new Date()
            day = today.getDate()
            month = today.getMonth()+1
            year = today.getFullYear()
        else
            dateSplit = date.split("/")
            day = dateSplit[0]
            month = dateSplit[1]
            year = dateSplit[2]

        tag = @tags.val()
        if tag == ""
            tag = "no tag"

        categoryClass = Lungo.dom("img#category")[0].className.split(" ")
        categoryId = getCategoryId(categoryClass[2])

        movementID = Lungo.Data.Storage.persistent("movementID")
        movementUID = Lungo.Data.Storage.persistent("movementUID")

        cleanAmount = @amount[0].innerText
        while (cleanAmount.toString().indexOf(user.currency.thousands_separator) != -1)
            cleanAmount = cleanAmount.toString().replace(user.currency.thousands_separator,"");
        cleanAmount = cleanAmount.replace(user.currency.decimal_mark,".")
        cleanAmount = cleanAmount.replace(currencySymbol,"")

        movementRecurrence = __Model.MovementRecurrence.find(Lungo.dom("section#NewMovement > article input#movement_recurrence").val())
        
        if movementID isnt null
            url = "http://www.whallet.com/api/v1/movement/update.json"
            post_data =
                token: Lungo.Data.Storage.persistent("tokenAuth"),
                year: year,
                month: month,
                day: day,
                amount: cleanAmount,
                description: @description.val(),
                category: categoryId,
                tags: tag,
                rule: movementRecurrence.rule,
                endson: movementRecurrence.endson,
                endson_input: movementRecurrence.endson_input,
                id: movementID

        else
            url = "http://www.whallet.com/api/v1/movement/new.json"
            console.log movementRecurrence.rule
            post_data =
                token: Lungo.Data.Storage.persistent("tokenAuth"),
                year: year,
                month: month,
                day: day,
                amount: cleanAmount,
                description: @description.val(),
                category: categoryId,
                tags: tag,
                rule: movementRecurrence.rule,
                endson: movementRecurrence.endson,
                endson_input: movementRecurrence.endson_input

        Lungo.Service.Settings.error = errorResponse
        result = Lungo.Service.post(url, post_data, saveResponse)

        # new movement memory
        
        dateSelected = new Date(Lungo.Data.Storage.persistent("dateSelected"))    
        date = new Date(year, month-1, day)


        movementOld = null 
        if movementUID isnt null
            movementOld = __Model.Movement.find(movementUID)
        movementModel = null
        if Helpers.getFormatDate(dateSelected) == Helpers.getFormatDate(date)
            description = if @description.val() then @description.val() else  l('%no_description')
            actualFormatDate = Helpers.getElegantDate(date)
            type = "expense"
            if categoryId == 10 or categoryId== "10" then type = "income"

            if movementID isnt null

                movementModel = movementOld
                

                movementModel.description = description
                movementModel.category= Helpers.getCategoryName(categoryId)
                movementModel.categoryID= categoryId
                movementModel.date= date
                movementModel.formatDate= actualFormatDate
                movementModel.tags= tag
                movementModel.amount=  Helpers.getFormatNumber(cleanAmount, currencySymbol)
                movementModel.amountDouble = parseFloat(cleanAmount)
                movementModel.type= type
                movementModel.done= false

                if movementModel.has_recurrence
                    movementModel.rule = movementRecurrence.rule
                    movementModel.rule_locale = Helpers.getTranslate("recurrence_" + movementRecurrence.rule)
                    movementModel.endson = movementRecurrence.endson
                    movementModel.endson_input = movementRecurrence.endson_input
                else
                    if movementRecurrence.rule isnt __Model.MovementRecurrence.RULES.NEVER
                        movementModel.rule = movementRecurrence.rule
                        movementModel.rule_locale = Helpers.getTranslate("recurrence_" + movementRecurrence.rule)
                        movementModel.endson = movementRecurrence.endson
                        movementModel.endson_input = movementRecurrence.endson_input
                        movementModel.has_recurrence = true

                movementModel.save()
            else
                movementModel = __Model.Movement.create
                    description: description,
                    category: Helpers.getCategoryName(categoryId),
                    categoryID: categoryId,
                    date: date,
                    formatDate: actualFormatDate,
                    tags: tag,
                    amount:  Helpers.getFormatNumber(cleanAmount, currencySymbol),
                    amountDouble: parseFloat(cleanAmount),
                    type: type,
                    rule_locale: Helpers.getTranslate("recurrence_" + __Model.MovementRecurrence.RULES.NEVER),
                    done: false

                if movementRecurrence.rule isnt __Model.MovementRecurrence.RULES.NEVER
                    movementModel.rule = movementRecurrence.rule
                    movementModel.rule_locale = Helpers.getTranslate("recurrence_" + movementRecurrence.rule)
                    movementModel.endson = movementRecurrence.endson
                    movementModel.endson_input = movementRecurrence.endson_input
                    movementModel.has_recurrence = true
                    movementModel.save()

                Lungo.Data.Storage.persistent("nuevo-movementUID", movementModel.uid)

            __Model.Spending.destroyAll()

        # recalculate balance from old movement date

        if movementOld isnt null

            if Helpers.getFormatDate(movementOld.date) isnt Helpers.getFormatDate(date)
                Lungo.Data.Storage.persistent("onlyRemove", "true")
                movementOld.destroy()

            for dataBalance in __Model.Balance.all()
                dateSplit = dataBalance.date.split('-')
                dateBalance = new Date(dateSplit[0], dateSplit[1] - 1, dateSplit[2])

                if((dateBalance.getMonth() == movementOld.date.getMonth()) and (dateBalance.getFullYear() == movementOld.date.getFullYear()))
                    if movementOld.categoryID == 10
                        dataBalance.income -= movementOld.amountDouble
                    else
                        dataBalance.expense -= movementOld.amountDouble
                    dataBalance.balance = dataBalance.income - dataBalance.expense
                    dataBalance.save()

        #recalcule balance from new movement date
        
        for dataBalance in __Model.Balance.all()
            dateSplit = dataBalance.date.split('-')
            dateBalance = new Date(dateSplit[0], dateSplit[1] - 1, dateSplit[2])

            if((dateBalance.getMonth() == date.getMonth()) and (dateBalance.getFullYear() == date.getFullYear()))
                if categoryId == 10
                    dataBalance.income += parseFloat(cleanAmount)
                else
                    dataBalance.expense += parseFloat(cleanAmount)
                dataBalance.balance = dataBalance.income - dataBalance.expense
                dataBalance.save()

        # reload balance of month

        dateSelectedFormat = Helpers.getFormatDate(new Date(dateSelected))
        for dataBalance in __Model.Balance.all()
            dateSplit = dataBalance.date.split('-')
            dateBalance = new Date(dateSplit[0], dateSplit[1] - 1, dateSplit[2])
            dataBalanceFormat = Helpers.getFormatDate(dateBalance)

            if(dataBalanceFormat == dateSelectedFormat)
                console.log "balance encontrado"
               
                dataBalance.income = 0
                dataBalance.expense = 0
                for movement in __Model.Movement.all()
                    if movement.categoryID == 10
                        dataBalance.income += movement.amountDouble
                    else
                        dataBalance.expense += movement.amountDouble
                dataBalance.balance = dataBalance.income - dataBalance.expense

                dataBalance.save()    

        # upate balance in localstorage
        WhalletBalances.saveToStorage()

        # new tag

        tagMovement = new __Model.Tag description: tag,category: categoryId

        encontradoTag = false
        for tag in __Model.Tag.all()
            if (tag.description is tagMovement.description) and (tag.category is tagMovement.category)
                encontradoTag = true

        if !encontradoTag
            tagMovement.save()
 
        Lungo.Notification.success(
            Helpers.getTranslate('success'),
            Helpers.getTranslate("movement_save"),
            "check",
            1,
            afterNotificationOnSave 
        )

    afterNotificationOnSave= () ->
        sectionBack = Lungo.Data.Storage.persistent("sectionReturnNoData")

        if sectionBack isnt null
            Lungo.Data.Storage.persistent("sectionReturnNoData", null)
            Lungo.Router.section(sectionBack)            
        else
            Lungo.Router.back()  

    errorResponse= (type, xhr) ->
        console.log "errorResponse"

        Lungo.Notification.hide()

        Lungo.Notification.error(
            "Error",
            Helpers.getTranslate("movement_error"),
            "cancel",
            3,
            null
        );

    deleteResponse= (response) ->
        console.log "deleteResponse"

        #Lungo.Data.Storage.persistent("balances", null)
        #Lungo.Data.Storage.persistent("expenseDistribution", null)
        #Lungo.Data.Storage.persistent("movements", null)
        Lungo.Data.Storage.persistent("movementID", null)

        #Lungo.Notification.success(
        #    Helpers.getTranslate('success'),
        #    Helpers.getTranslate("movement_deleted"),
        #    "check",
        #    3,
        #    afterNotification 
        #)

    afterNotification= () ->
        Lungo.Router.back()

    saveResponse= (response) ->
        console.log "saveResponse"        

        movementUID = Lungo.Data.Storage.persistent("nuevo-movementUID")

        if movementUID isnt null

            movementModelFind = __Model.Movement.find(movementUID)
            movementModelFind.id = response._id
            movementModelFind.save()

        Lungo.Data.Storage.persistent("balances", null)
        #Lungo.Data.Storage.persistent("expenseDistribution", null)
        Lungo.Data.Storage.persistent("movements", null)
        #sectionBack = Lungo.Data.Storage.persistent("sectionReturnNoData")

        #if sectionBack isnt null
        #    Lungo.Data.Storage.persistent("sectionReturnNoData", null)
        #    Lungo.Router.section(sectionBack)            
        #else
        #    Lungo.Router.back()

    helperTagsResponse= (response) ->
        console.log "helperTagsResponse"

        __Model.Tag.destroyAll()
        for dataResponse in response
            for tag in dataResponse.tags
                tagModel = __Model.Tag.create
                    description: tag.name,
                    category: dataResponse.category

    onChangeKindOf: (event)->
        console.log "onChangeKingOf"

        categoryId = 0
        Lungo.dom("a#income").removeClass()
        Lungo.dom("a#expense").removeClass()


        if(event.srcElement.id is "income")
            Lungo.dom("a#income").addClass("active-income")
            Lungo.dom("section#NewMovement > article div.category img").removeClass();
            Lungo.dom("section#NewMovement > article div.category img").addClass("icon whallet add-money");
            Lungo.dom("img#category")[0].src = "assets/images/categories/add-money.png"
            categoryId = 10
        else
            Lungo.dom("a#expense").addClass("active-expense")
            Lungo.dom("section#NewMovement > article div.category img").removeClass();
            Lungo.dom("section#NewMovement > article div.category img").addClass("icon whallet tag-2");
            Lungo.dom("img#category")[0].src = "assets/images/categories/wtag.png"

        Lungo.dom("div#tags")[0].innerHTML = ""
        Lungo.dom("ul#bullets")[0].innerHTML = ""
        bulletsList = ""
        bulletsList = "<li class='prev'><a href='#' data-action='carousel' data-direction='left' data-icon='left' onclick= carousel.prev()><span class='icon left'></span></a></li>"

        tags = __Model.Tag.all()
        tagsList = ""
        first = true
        tagsByCategory = []
        for tag in tags
            if tag.category == categoryId
                tagsByCategory.push tag

        while tagsByCategory.length
            smallTagsList = tagsByCategory.splice(0,6)
            if first
                bulletsList += "<li class='selected'><span class='bullet'>o</span></li>"
            else
                bulletsList += "<li class=''><span class='bullet'>o</span></li>"
            first = false
            tagsList += "<div align='center'>"
            for tag in smallTagsList
                tagsList += "<a href='#' class='button-tag' onclick=document.getElementById('txt-tag').value='"+tag.description+"'>"+ tag.description + "</a>"
            tagsList += '</div>'

        bulletsList += "<li class='next'><a href='#' data-action='carousel' data-direction='right' data-icon='right' onclick=carousel.next()><span class='icon right'></span></a></li>"
        Lungo.dom("div#tags")[0].innerHTML = tagsList
        Lungo.dom("ul#bullets")[0].innerHTML = bulletsList

controller_newMovement = new NewMovementCtrl "section#NewMovement"