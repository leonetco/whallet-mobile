class NewRecurrency extends Monocle.Controller
    
    events:
        "load section#newRecurrency"        : "onLoadNewRecurrency"
        "tap h4#recurrence_never_text"      : "onSelectRuleNever"
        "tap div.rule"                      : "onSelectRule"
        "tap div.endson"                    : "onSelectEndson"
        "tap a#recurrence_accept"           : "onSave"
        "tap a[data-action=cancel]"         : "onCancel"        

    elements:
        "#title_new_recursive"              :   "title_new_recursive"
        "#title_recursive_when"             :   "title_recursive_when"
        "#title_recursive_until"            :   "title_recursive_until"
        "#end-time-input"                   :   "end_time_input"
        "#number-repeat-input"              :   "number_repeat_input"
        "#recurrence_never_text"            :   "recurrence_never_text"
        "#recurrence_daily_text"            :   "recurrence_daily_text"
        "#recurrence_monthly_text"          :   "recurrence_monthly_text"
        "#recurrence_yearly_text"           :   "recurrence_yearly_text"
        "#recurrence_daily_text2"           :   "recurrence_daily_text2"
        "#recurrence_monthly_text2"         :   "recurrence_monthly_text2"
        "#recurrence_yearly_text2"          :   "recurrence_yearly_text2"
        "#recurrence_until_forever_text"    :   "recurrence_until_forever_text"
        "#recurrence_until_date_text"       :   "recurrence_until_date_text"
        "#recurrence_until_on_text"         :   "recurrence_until_on_text"
        "#recurrence_cancel"                :   "recurrence_cancel"
        "#recurrence_accept"                :   "recurrence_accept"

    onLoadNewRecurrency: (event) ->
        console.log "onLoadNewRecurrency"
        
        @title_new_recursive[0].innerHTML = l("%recurrence_title")
        @title_recursive_when[0].innerHTML = l("%recurrence_when")
        @title_recursive_until[0].innerH#TML = l("%recurrence_until")
        @recurrence_never_text[0].innerHTML = l("%recurrence_nunca")
        @recurrence_daily_text[0].innerHTML = l("%recurrence_every")
        @recurrence_monthly_text[0].innerHTML = l("%recurrence_every")
        @recurrence_yearly_text[0].innerHTML = l("%recurrence_every")
        @recurrence_daily_text2[0].innerHTML = l("%recurrence_Daily")
        @recurrence_monthly_text2[0].innerHTML = l("%recurrence_Monthly")
        @recurrence_yearly_text2[0].innerHTML = l("%recurrence_Yearly")
        @recurrence_until_forever_text[0].innerHTML = l("%recurrence_until_forever")
        @recurrence_until_date_text[0].innerHTML = l("%recurrence_until_date")
        @recurrence_until_on_text[0].innerHTML = l("%recurrence_until_on")
        @recurrence_cancel[0].innerHTML = l("%cancel")
        @recurrence_accept[0].innerHTML = l("%accept")
        
        clearEndsonInput()
        Lungo.dom(".active-item").removeClass('active-item')
        @recurrence_never_text.removeClass('active-red')

        movementRecurrence = __Model.MovementRecurrence.find(Lungo.dom("section#NewMovement > article input#movement_recurrence").val())
        
        console.log movementRecurrence

        if movementRecurrence.rule is __Model.MovementRecurrence.RULES.NEVER
            @recurrence_never_text.addClass('active-red')
        else
            Lungo.dom("#recurrence_"+movementRecurrence.rule).addClass('active-item')

            Lungo.dom("#endson_" + movementRecurrence.endson).addClass("active-item")

            if movementRecurrence.endson is __Model.MovementRecurrence.ENDSON.UNTIL
                $$("h6#end-time-input")[0].innerHTML = movementRecurrence.endson_input

            if movementRecurrence.endson is __Model.MovementRecurrence.ENDSON.COUNT
                @number_repeat_input[0].innerHTML =  movementRecurrence.endson_input.toString()
        

    onSave: (event) ->
        console.log "onSave"

        movementRecurrence = __Model.MovementRecurrence.find(Lungo.dom("section#NewMovement > article input#movement_recurrence").val())

        rule = __Model.MovementRecurrence.RULES.NEVER
        rule_element =  document.getElementsByClassName('rule active-item');
        if (rule_element.length > 0)      
            rule = rule_element[0].getAttribute("data-rule")
        
        movementRecurrence.rule = rule
        movementRecurrence.rule_locale = Helpers.getTranslate("recurrence_"+rule)                
        movementRecurrence.endson = __Model.MovementRecurrence.ENDSON.FOREVER
        movementRecurrence.endson_input = ""        

        if document.getElementsByClassName('endson active-item').length
            endson = document.getElementsByClassName('endson active-item')[0].id
            if endson is "endson_" + __Model.MovementRecurrence.ENDSON.UNTIL
                movementRecurrence.endson = __Model.MovementRecurrence.ENDSON.UNTIL 
                movementRecurrence.endson_input = @end_time_input[0].innerHTML        
            else if endson is  "endson_" + __Model.MovementRecurrence.ENDSON.COUNT
                movementRecurrence.endson = __Model.MovementRecurrence.ENDSON.COUNT
                movementRecurrence.endson_input = @number_repeat_input[0].innerHTML
        
        movementRecurrence.save()
        Lungo.dom("section#NewMovement > article span.reload")[0].innerHTML = movementRecurrence.rule_locale        
 
        Lungo.Router.back();
        
    onCancel: (event) ->
        console.log "onCancel"
        Lungo.Router.back()

    onSelectRuleNever: (event) ->
        console.log "onSelectRuleNever"        
        @recurrence_never_text.addClass('active-red')
        Lungo.dom(".rule").removeClass('active-item')        

    onSelectRule: (event) ->
        console.log "onSelectOption"
        @recurrence_never_text.removeClass('active-red')
        Lungo.dom(".rule").removeClass('active-item')
        Lungo.dom("#"+event.currentTarget.id).addClass('active-item')

    onSelectEndson: (event) ->
        console.log "onSelectEndson"

        Lungo.dom(".endson").removeClass('active-item')
        Lungo.dom("#"+event.currentTarget.id).addClass('active-item')
        #clearEndsonInput()

        if event.currentTarget.id is "endson_" + __Model.MovementRecurrence.ENDSON.FOREVER      
            Lungo.dom("#end-time-input")[0].innerHTML = ""
            Lungo.dom("#number-repeat-input")[0].innerHTML = ""

        if event.currentTarget.id is "endson_" + __Model.MovementRecurrence.ENDSON.UNTIL
            setCalendar()

        if event.currentTarget.id is "endson_" + __Model.MovementRecurrence.ENDSON.COUNT            
            setNumberRepeat()

    clearEndsonInput= () ->
        Lungo.dom("#end-time-input")[0].innerHTML = ""
        Lungo.dom("#number-repeat-input").val("")

    setNumberRepeat= () ->
        console.log "setNumberRepeat"

        number = "1"

        if Lungo.dom("h6#number-repeat-input")[0].innerText 
            number = Lungo.dom("h6#number-repeat-input")[0].innerText
        
        Lungo.Notification.confirm({
            icon: '',
            title: "Selecciona una cantidad",
            description:  Helpers.getTemplateSelectNumber(number),
            accept: {
                icon: 'checkmark',
                label: Helpers.getTranslate('accept'),
                callback: (results) ->                    
                    Lungo.dom("#end-time-input")[0].innerHTML = ""
                    $$("h6#number-repeat-input")[0].innerHTML = Lungo.dom("div#yearCalendarMobile")[0].innerText
            },
            cancel: {
                icon: 'close',
                label: Helpers.getTranslate('cancel'),
                callback: null
            }
        })        
    
    setCalendar= () ->
        console.log "setCalendar"

        fechaCalendario = new Date(Lungo.Data.Storage.persistent("dateSelected"))

        if Lungo.dom("#end-time-input")[0].innerHTML 
            fechaCalendario = Helpers.to_date(Lungo.dom("#end-time-input")[0].innerHTML)

        Lungo.Notification.confirm({
            icon: '',
            title: Helpers.getTranslate("select_date"),
            description:  Helpers.getTemplateSelectDayMonthYear(fechaCalendario),
            accept: {
                icon: 'checkmark',
                label: Helpers.getTranslate('accept'),
                callback: (results) ->
                    
                    Lungo.dom("#number-repeat-input")[0].innerHTML = ""
                    console.log "accept onChangeDate"
                    date = Calendar.getActualDate() 
                    $$("h6#end-time-input")[0].innerHTML = Helpers.getLongFormatDate(date)
            },
            cancel: {
                icon: 'close',
                label: Helpers.getTranslate('cancel'),
                callback: null
            }
        })


controller_recurrency = new NewRecurrency "section#newRecurrency"