class MovementsCtrl extends Monocle.Controller

    events:
        "load section#movements"    : "onLoadMovements"
        "tap h4#movements_date"     : "onChangeDate"
        "tap span.icon.menu"        : "onChangeDate"

    elements:
        "#movements_date"           :   "movements_date"
        "#title_movements"           :   "title_movements"

    constructor: ->
        super
        __Model.Movement.bind "destroy",  @bindMovementDestroy

    bindMovementDestroy: (movement) ->
        console.log "bindMovementDestroy"
        #document.getElementById("delete_tag_"+ event.currentTarget.id).className = "right tag cancel"

        onlyRemove = Lungo.Data.Storage.persistent("onlyRemove", "true")

        if onlyRemove is null

            url = "http://www.whallet.com/api/v1/movement/delete.json"
            date = new Date()
            post_data =
                token: Lungo.Data.Storage.persistent("tokenAuth"),
                id: movement.id

            Lungo.Service.Settings.error = errorResponse
            result = Lungo.Service.post(url, post_data, deleteResponse)
        Lungo.Data.Storage.persistent("onlyRemove", null)

    errorResponse= (type, xhr) ->
        console.log "errorResponse"
        console.log("xhr: %o", xhr)
        Lungo.Notification.error(
            "Error",
            Helpers.getTranslate("error_msg"),
            "cancel",
            3,
            null
        )

    deleteResponse= (response) ->
        console.log "deleteResponse"

        Lungo.Data.Storage.persistent("balances", null)
        #Lungo.Data.Storage.persistent("expenseDistribution", null)
        Lungo.Data.Storage.persistent("movements", null)

        Lungo.Notification.success(
            "Success",
            Helpers.getTranslate("movement_deleted"),
            "check",
            3,
            null
        )

    onChangeDate: (event) ->
        console.log "onChangeDate"

        fechaCalendario = new Date(Lungo.Data.Storage.persistent("dateSelected"))

        Lungo.Notification.confirm({
            icon: '',
            title: Helpers.getTranslate("select_date"),
            description:  Helpers.getTemplateSelectMonthYear(fechaCalendario),
            accept: {
                icon: 'checkmark',
                label: Helpers.getTranslate('accept'),
                callback: (results) ->
                    console.log "accept onChangeDate" 
                    date = Calendar.getActualDate()
                    Lungo.Data.Storage.persistent("dateSelected", null)
                    Lungo.Data.Storage.persistent("dateSelected", date)

                    $$("h4#movements_date")[0].innerHTML = Helpers.getFormatDate(date)
                    
                    #Lungo.Data.Storage.persistent("movements", null)

                    Lungo.Notification.show()
                    url = "http://www.whallet.com/api/v1/movements.json"
                    post_data =
                        token: Lungo.Data.Storage.persistent("tokenAuth"),
                        year: date.getFullYear(),
                        month: date.getMonth()+1

                    result = Lungo.Service.post(url, post_data, movementsServiceResponse)
            },
            cancel: {
                icon: 'close',
                label: Helpers.getTranslate('cancel'),
                callback: null
            }
        })

    movementsServiceResponse= (response) ->
        WhalletMovements.loadFromResponse response

        movements = __Model.Movement.all()

        movements.sort (a, b) -> 
            a = new Date(a.date)
            b = new Date(b.date)

            res = 0
            if a < b
                res = 1
            if a > b
                res = -1

            res            

        Lungo.Notification.hide()

        movementsResponse movements

    onLoadMovements: (event) ->
        console.log "onLoadMovements"

        @title_movements[0].innerHTML = l("%movements")        
        Lungo.Data.Storage.persistent("movementID", null)
        Lungo.Data.Storage.persistent("movementUID", null)

        movements = __Model.Movement.all()   
        console.log movements     

        movements.sort (a, b) -> 
            a = new Date(a.date)
            b = new Date(b.date)

            res = 0
            if a < b
                res = 1
            if a > b
                res = -1

            res            

        movementsResponse movements

    movementsResponse= (response) ->
        console.log "movementsResponse"

        currencySymbol = Helpers.userCurrencySymbol()
       
        date = Lungo.Data.Storage.persistent("dateSelected")
        @movements_date.innerHTML = Helpers.getFormatDate(new Date(date))

        # clear list
        $$("ul#movements").html(' ')
        $$("div#distribution_no_data").addClass("hidden")
        $$("article#movements").addClass("indented")
        if movements.length == 0
            $$("div#distribution_no_data").removeClass()
            $$("article#movements").removeClass("indented")
        else

        formatDate = ""
        for movement in response

            actualFormatDate = movement['formatDate']
            if formatDate isnt actualFormatDate
                formatDate = actualFormatDate
                viewHeader = new __View.MovementHeaderList model: movement
                viewHeader.append movement

            view = new __View.MovementItemList model: movement
            view.append movement

        Lungo.Notification.hide()

controller_movements = new MovementsCtrl "section#movements"