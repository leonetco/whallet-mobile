class SettingsCtrl extends Monocle.Controller

    events:
        "load section#settings"     : "onLoadSetting"        
        "tap li#currencyActive"     : "onChangeCurrency"
        "tap li#usernameActive"     : "onChangeUsername"
        "tap li#localeActive"     : "onChangeLocale"

    elements:
        "#username"     :   "username"
        "#userCurrency" :   "userCurrency"
        "#userLocale"   :   "userLocale"
        "#title_settings" :   "title_settings"
        "#settings_username" :   "settings_username"
        "#settings_currency" :   "settings_currency"
        "#settings_locale" :   "settings_locale"
        
    onChangeLocale: (event) ->
        console.log "onChangeLocale"
    
        Lungo.Router.section("locales")

    onChangeUsername: (event) ->
        console.log "onChangeUsername" 
        
        userInfo = Lungo.Data.Storage.persistent("userInfo")        
        name = ""

        if userInfo.name?# isnt null
            name = userInfo.name 

        template = "<form style=\"width: 100%\">"
        template += "<fieldset style=\"border-bottom: 2px solid #0093D5\">"
        template += "<span id=\"screen-input\" style=\"font-size: 1.4em; height:40px; margin-top:15px\" class=\"text thin left\">"
        template += "<abbr><input type=\"text\" id=\"usernameChange\" value=\"#{name}\"></abbr>"
        template += "</span></fieldset></form>"

        Lungo.Notification.confirm({
            icon: '',
            title: Helpers.getTranslate("insert_name"),
            description: template,
            accept: {
                icon: 'checkmark',
                label: Helpers.getTranslate("accept"),
                callback: ->
                    inputScreen = Lungo.dom("#usernameChange")[0].value                    
                    Lungo.dom("section#settings > article small#username")[0].innerText = inputScreen;

                    tokenStored = Lungo.Data.Storage.persistent("tokenAuth")
                    url = "http://www.whallet.com/api/v1/user/update.json"
                    post_data = 
                        token: tokenStored,
                        name: inputScreen
                    Lungo.Service.Settings.error = errorResponse            
                    result = Lungo.Service.post(url, post_data, userUpdateResponse)
            },
            cancel: {
                icon: 'close',
                label: Helpers.getTranslate("cancel"),
                callback: null
            }
        });

    userUpdateResponse= (response) ->
        console.log "userUpdateResponse"
        Lungo.Data.Storage.persistent("userInfo", response)


    errorResponse= (type, xhr) ->
        console.log "errorResponse"
        console.log("xhr: %o", xhr)
        Lungo.Notification.error(
            Helpers.getTranslate("error"),
            Helpers.getTranslate("error_msg"),
            "cancel",
            3,
            null
        )

    onChangeCurrency: (event) ->
        console.log "onChangeCurrency" 

        Lungo.Router.section("currencies")

    onLoadSetting: (event) ->
        console.log "onLoadSetting"        

        @title_settings[0].innerHTML = l("%settings") 
        @settings_username[0].innerHTML = l("%username") 
        @settings_currency[0].innerHTML = l("%currency") 
        @settings_locale[0].innerHTML = l("%settings_locale")
        username = User.getUserName()
        @username[0].innerText = username
        @username[0].innerHTML = username

        currency = User.getCurrency()
        @userCurrency[0].innerText = currency
        @userCurrency[0].innerHTML = currency

        @userLocale[0].innerText = l("%locale_"+User.getLocale())

controller_monitor = new SettingsCtrl "section#settings"