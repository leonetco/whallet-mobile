class SplashCtrl extends Monocle.Controller

    events:
        "load section#splash"               : "onLoadSplash"

    onLoadSplash: (event) ->
        console.log "onLoadSplash"
    
        date = new Date()
        Lungo.Data.Storage.persistent("dateSelected", null)
        Lungo.Data.Storage.persistent("movementID", null)
        Lungo.Data.Storage.persistent("movementUID", null)
        Lungo.Data.Storage.persistent("nuevo-movementUID", null)
        Lungo.Data.Storage.persistent("dateSelected", date)

        tokenStored = Lungo.Data.Storage.persistent("tokenAuth")

        if tokenStored isnt null            

            # UserInfo
            userInfo = Lungo.Data.Storage.persistent("userInfo")
            if userInfo isnt null                
                String.locale = User.getUserInfo().locale
                Helpers.updateAllTranslateElement()

            # Recovery tags
            tags = Lungo.Data.Storage.persistent("tags")
            if tags isnt null
                tags = WhalletTags.loadFromStorage()                

            #  2 - Spending -> Storage    

            balances = Lungo.Data.Storage.persistent("balances")
            if balances isnt null
                balances = WhalletBalances.loadFromStorage()
                Lungo.Router.section("monitor")

            url = "http://www.whallet.com/api/v1/dashboard/balance.json"
            post_data = token: tokenStored
            result = Lungo.Service.post(url, post_data, balanceResponse)

            #  4- Movements -> Storage
            Lungo.Data.Storage.persistent("movements", null)
            url = "http://www.whallet.com/api/v1/movements.json"

            post_data =
                token: tokenStored,
                year: date.getFullYear(),
                month: date.getMonth()+1

            result = Lungo.Service.post(url, post_data, movementsResponse)            


            # User Info Call
            url = "http://www.whallet.com/api/v1/user/info.json"
            post_data = 
                token: tokenStored
            result = Lungo.Service.post(url, post_data, userInfoResponse)            



            url = "http://www.whallet.com/api/v1/helper/tags.json"
            post_data = token: tokenStored
            result = Lungo.Service.post(url, post_data, helperTagsResponse)



            #  3 - Expense Distribution
            #Lungo.Data.Storage.persistent("expenseDistribution", null)
            #url = "http://www.whallet.com/api/v1/dashboard/spending.json"
            #post_data =
            #    token: tokenStored
            #result = Lungo.Service.post(url, post_data, spendingResponse)


            # Currencies
            Lungo.Data.Storage.persistent("currencies", null)
            url = "http://www.whallet.com/api/v1/helper/currencies.json"
            userInfo = User.getUserInfo()
            locale = "es"
            if userInfo isnt null
                locale = User.getUserInfo().locale 
            post_data = 
                locale:  locale  
            result = Lungo.Service.get(url, post_data, currenciesResponse)

        else
            Lungo.Data.Storage.persistent("token",null)
            Lungo.Data.Storage.persistent("tokenAuth",null)
            Lungo.Data.Storage.persistent("user",null)
            Lungo.Data.Storage.persistent("userInfo",null)
            Lungo.Data.Storage.persistent("balances", null)
            Lungo.Data.Storage.persistent("movements", null)
            Lungo.Data.Storage.persistent("expenses", null)
            __Model.Movement.destroyAll()
            __Model.Spending.destroyAll()
            __Model.Balance.destroyAll()
            __Model.Tag.destroyAll()
            
            Lungo.Router.section("home")

    errorResponse= (type, xhr) ->
        console.log "errorResponse"
        console.log("xhr: %o", xhr)
        Lungo.Notification.error(
            Helpers.getTranslate("error_unknown"),
            Helpers.getTranslate("check_inet"),
            "broadcast",
            5,
            null
        )


    movementsResponse= (response) ->
        console.log "Splash.movementsResponse"
        WhalletMovements.loadFromResponse response

    balanceResponse= (response) ->
        console.log "Splash.balanceResponse"
        balance = Lungo.Data.Storage.persistent("balances")
        
        __Model.Balance.destroyAll()
        for dataResponse in response
            balanceModel = new __Model.Balance
            balanceModel.date = dataResponse.date
            balanceModel.expense = dataResponse.data.expense
            balanceModel.income = dataResponse.data.income
            balanceModel.balance = dataResponse.data.balance  
            balanceModel.save()  
            
        WhalletBalances.saveToStorage()

        if balance is null
            Lungo.Router.section("monitor")

    helperTagsResponse= (response) ->
        console.log "helperTagsResponse"

        __Model.Tag.destroyAll()

        for dataResponse in response
            for tag in dataResponse.tags
                tagModel = __Model.Tag.create
                    description: tag.name,
                    category: dataResponse.category

        WhalletTags.saveToStorage()

    currenciesResponse= (response) ->
        console.log "currenciesResponse"
        Lungo.Data.Storage.persistent("currencies", response)

    userInfoResponse= (response) ->
        console.log "userInfoResponse"
        
        if response?    
            String.locale = response.locale    
            Helpers.updateAllTranslateElement()
            Lungo.Data.Storage.persistent("userInfo", null)
            Lungo.Data.Storage.persistent("userInfo", response)


controller_splash = new SplashCtrl "section#splash"
