class __View.MovementItemList extends Monocle.View

    container: "ul#movements"

    template:
        """
        <li id="{{id}}" class="thumb big selectable" style="padding-bottom: 15px; margin-bottom: 0px">
            <img src="assets/images/categories/{{category}}.png">
            <div class="right text {{type}}"><h4>{{amount}}</h4></div>
            <strong><span class="text bold">{{tags}} </span></strong>
            <small>
                {{description}}
            </small>
            {{#has_recurrence}}<div class="right"><span class="icon androidicon-2 reload"></span><span>{{rule_locale}}</span></div>{{/has_recurrence}}
            
        </li>
        """

    events:
        "tap li"        : "onEdit"

    onEdit: (event) ->
        console.log "onEdit.view"

        currencySymbol = Helpers.userCurrencySymbol()

        Lungo.dom("a#deleteMovement").removeClass()
        Lungo.dom("a#income").removeClass()
        Lungo.dom("a#expense").removeClass()

        if @model.category == "add-money"
            Lungo.dom("a#income").addClass("active-income")
        else
            Lungo.dom("a#expense").addClass("active-expense")

        Lungo.dom("img#category").removeClass()
        Lungo.dom("img#category").addClass("icon whallet "+ @model.category)
        categoryName = @model.category

        if @model.category == "tag-2"
            categoryName = "wtag"

        Lungo.dom("img#category")[0].src = "assets/images/categories/"+categoryName+".png"        


        currencyActive = User.getCurrencyActive()
        amountClean = @model.amount.replace(" " + currencySymbol, "")
        amountClean = amountClean.replace(currencySymbol + " ", "")

        Lungo.dom("section#NewMovement > article span#amount")[0].innerText = currencySymbol + " " + amountClean
        description = ""
        if @model.description !=  l('%no_description')
            description = @model.description

        Lungo.dom("section#NewMovement > article input#txt-description")[0].value = description
        Lungo.dom("section#NewMovement > article input#txt-tag")[0].value = @model.tags

        currentDate = new Date(@model.date)
        dd = currentDate.getDate()
        mm = currentDate.getMonth() + 1
        yyyy = currentDate.getFullYear()
        if (dd < 10)
            dd = "0" + dd

        if (mm < 10)
            mm = "0" + mm

        currentDate = dd + '/' + mm + '/' + yyyy
        Lungo.dom("section#NewMovement > article input#txt-date")[0].value = currentDate


        __Model.MovementRecurrence.destroyAll();

        rule_locale = Helpers.getTranslate("recurrence_" + __Model.MovementRecurrence.RULES.NEVER)
        if @model.rule
            console.log "@model.rule no es nulo"
            rule_locale = Helpers.getTranslate("recurrence_" + @model.rule)

        recurrence = __Model.MovementRecurrence.create({
          rule: @model.rule,
          rule_locale: rule_locale,
          endson: @model.endson,
          endson_input : @model.endson_input          
        });

        Lungo.dom("section#NewMovement > article input#movement_recurrence").val(recurrence.uid)
        console.log recurrence
        if @model.has_recurrence
            Lungo.dom("section#NewMovement > article span.reload")[0].innerHTML = recurrence.rule_locale
        else
            Lungo.dom("section#NewMovement > article span.reload")[0].innerHTML = ""
            
        Lungo.Data.Storage.persistent("movementID", @model.id)
        Lungo.Data.Storage.persistent("movementUID", @model.uid)
        Lungo.Router.section("NewMovement")
    onDelete: (event) ->
        console.log "onDelete.view"
        #if navegador.onLine
        @remove()