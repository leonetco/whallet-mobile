class __View.MovementHeaderList extends Monocle.View

    container: "ul#movements"

    template:
        """
        <li class="header" style="background-color: #AFD4D4; padding: 5px 10px !important; margin-bottom: 0px; margin-top: 10px">
            <h5 style="color: #ffffff" class="text book"> {{formatDate}}</h5>
        </li>
        """